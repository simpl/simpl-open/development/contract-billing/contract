package eu.europa.ec.simpl.contracts.kafka;

import eu.europa.ec.simpl.contracts.kafka.events.ContractAgreementRequestEvent;
import eu.europa.ec.simpl.contracts.kafka.events.ContractAgreementResponseEvent;
import eu.europa.ec.simpl.contracts.service.ContractAgreementService;
import eu.europa.ec.simpl.contracts.service.SignContractService;
import eu.europa.ec.simpl.contracts.transfer.Mode;
import eu.europa.ec.simpl.contracts.transfer.SignContractAgreementRequestTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SignContractRequestConsumer {

    private final MessageProducer messageProducer;
    private final MessageMapper messageMapper;
    private final ContractAgreementService contractAgreementService;
    private final SignContractService signContractService;

    @Value("${spring.kafka.topics.sign-contract-response}")
    private String signContractResponseTopic;
    @Value("${spring.mode}")
    private String mode;

    public SignContractRequestConsumer(MessageProducer messageProducer, MessageMapper messageMapper, ContractAgreementService contractAgreementService, SignContractService signContractService) {
        this.messageProducer = messageProducer;
        this.messageMapper = messageMapper;
        this.contractAgreementService = contractAgreementService;
        this.signContractService = signContractService;
    }

    @KafkaListener(topics = "${spring.kafka.topics.sign-contract-request}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(String message) {
        log.info("Received message: " + message);
        ContractAgreementRequestEvent event = messageMapper.createEvent(message, ContractAgreementRequestEvent.class);
        // store data record before signature
        if (Mode.PROVIDER.name().equals(mode)) {
            contractAgreementService.createAndSaveContractAgreement(event.getContractAgreementId(), event.getContractDefinitionId(), event.getContractAgreementCreateTO());
        }
        boolean signed = false;
        try {
            // prepare request
            // todo: prepare request using contractAgreementId and contractDefinitionId and parse response to get status
            SignContractAgreementRequestTO request = signContractService.generateRequest();

            // sign contract
            String response = signContractService.sendToSigner(request);

            // store info about signature for provider
            if (Mode.PROVIDER.name().equals(mode)) {
                contractAgreementService.updateContractAgreementProviderDateAndStatus(event.getContractAgreementId());
            }
            signed = true;
        } catch (Exception e) {
            log.error("Failed to sign contract", e);
        }

        // send response to kafka (producer)
        ContractAgreementResponseEvent responseEvent = new ContractAgreementResponseEvent(event.getContractAgreementId(),
                event.getContractAgreementCreateTO().getContractNegotiationId(),
                Mode.valueOf(mode), signed);
        messageProducer.sendMessage(signContractResponseTopic, messageMapper.convertEventIntoStringMessage(responseEvent));
        log.info("Contract signed for ID: {}", event.getContractAgreementId());
    }
}
