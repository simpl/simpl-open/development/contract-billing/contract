package eu.europa.ec.simpl.contracts.types;

public enum ContractAgreementStatusType {

    INITIATED,
    CREATED,
    FINALIZING,
    FINALIZED,
    TERMINATED

}
