package eu.europa.ec.simpl.contracts.repository;

import eu.europa.ec.simpl.contracts.entity.ContractAgreement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ContractAgreementRepository extends JpaRepository<ContractAgreement, UUID> {

}
