package eu.europa.ec.simpl.contracts.service;

import eu.europa.ec.simpl.common.exceptions.BadArgumentException;
import eu.europa.ec.simpl.common.exceptions.RecordNotFoundException;
import eu.europa.ec.simpl.contracts.entity.ContractAgreement;
import eu.europa.ec.simpl.contracts.kafka.MessageMapper;
import eu.europa.ec.simpl.contracts.kafka.MessageProducer;
import eu.europa.ec.simpl.contracts.kafka.events.ContractAgreementRequestEvent;
import eu.europa.ec.simpl.contracts.kafka.events.StatusUpdateRequestEvent;
import eu.europa.ec.simpl.contracts.mapper.ContractAgreementMapper;
import eu.europa.ec.simpl.contracts.mapper.ContractMapper;
import eu.europa.ec.simpl.contracts.repository.ContractAgreementRepository;
import eu.europa.ec.simpl.contracts.transfer.ContractAgreementCreateTO;
import eu.europa.ec.simpl.contracts.transfer.ContractAgreementTO;
import eu.europa.ec.simpl.contracts.transfer.ContractResponseTO;
import eu.europa.ec.simpl.contracts.transfer.Mode;
import eu.europa.ec.simpl.contracts.types.ContractAgreementStatusType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@ComponentScan("eu.europa.ec.simpl.common.events")
@Slf4j
public class ContractAgreementService {

    private final ContractAgreementRepository contractAgreementRepository;
    private final MessageProducer messageProducer;
    private final MessageMapper messageMapper;
    private final ContractAgreementFileStorageService contractAgreementFileStorageService;

    @Value("${spring.kafka.topics.sign-contract-request}")
    private String signContractRequestTopic;
    @Value("${spring.kafka.topics.status-update}")
    private String statusUpdateTopic;
    @Value("${spring.mode}")
    private String mode;

    public ContractAgreementService(ContractAgreementRepository contractAgreementRepository, MessageProducer messageProducer, MessageMapper messageMapper, ContractAgreementFileStorageService contractAgreementFileStorageService) {
        this.contractAgreementRepository = contractAgreementRepository;
        this.messageProducer = messageProducer;
        this.messageMapper = messageMapper;
        this.contractAgreementFileStorageService = contractAgreementFileStorageService;
    }

    @Transactional
    public void updateContractStatus(UUID contractAgreementId, String contractDefinitionId, ContractAgreementStatusType status) {
        Optional<ContractAgreement> contractAgreementOpt = contractAgreementRepository.findById(contractAgreementId);
        if (!contractAgreementOpt.isPresent()) {
            log.info("ContractAgreement not found for contractAgreementId: {}", contractAgreementId);
        } else {
            ContractAgreement contractAgreement = contractAgreementOpt.get();
            validateContractAgreement(contractDefinitionId, contractAgreement);
            contractAgreement.setStatus(status);
            contractAgreementRepository.save(contractAgreement);
            if (status == ContractAgreementStatusType.FINALIZED) {
                contractAgreementFileStorageService.storeFileForContractAgreement(contractAgreement);
            }
        }
    }

    @Transactional
    public void createAndSaveContractAgreement(UUID contractAgreementId, String contractDefinitionId, ContractAgreementCreateTO contractAgreementCreateTO) {
        ContractAgreement contractAgreement = new ContractAgreement();
        contractAgreement.setContractAgreementId(contractAgreementId);
        contractAgreement.setContractDefinitionId(contractDefinitionId);
        contractAgreement.setStatus(ContractAgreementStatusType.INITIATED);
        contractAgreement.setConsumerSignatureDate(LocalDateTime.now());
        if (contractAgreementCreateTO != null) {
            contractAgreement.setContractNegotiationId(contractAgreementCreateTO.getContractNegotiationId());
            contractAgreement.setContractOfferId(contractAgreementCreateTO.getContractOfferId());
            contractAgreement.setAssetId(contractAgreementCreateTO.getAssetId());
            contractAgreement.setProviderId(contractAgreementCreateTO.getProviderId());
            contractAgreement.setConsumerId(contractAgreementCreateTO.getConsumerId());
        }
        contractAgreementRepository.save(contractAgreement);
    }

    @Transactional
    public void updateContractAgreementProviderDateAndStatus(UUID contractAgreementId) {
        ContractAgreement contractAgreement = contractAgreementRepository.findById(contractAgreementId).get();
        contractAgreement.setStatus(ContractAgreementStatusType.CREATED);
        contractAgreement.setProviderSignatureDate(LocalDateTime.now());
        contractAgreementRepository.save(contractAgreement);
    }

    public ContractResponseTO sendStatusUpdateRequest(UUID contractAgreementId, String contractDefinitionId, ContractAgreementStatusType status) {
        StatusUpdateRequestEvent event = new StatusUpdateRequestEvent(contractAgreementId, contractDefinitionId, Mode.valueOf(mode), status);
        messageProducer.sendMessage(statusUpdateTopic, messageMapper.convertEventIntoStringMessage(event));
        return ContractMapper.INSTANCE.mapToTO(event, ContractAgreementStatusType.FINALIZING);
    }

    public ContractResponseTO issueVerifiableCredential(UUID contractAgreementId, String contractDefinitionId, ContractAgreementCreateTO contractAgreementCreateTO) {
        ContractAgreementRequestEvent event = new ContractAgreementRequestEvent(contractAgreementId, contractDefinitionId, Mode.valueOf(mode), contractAgreementCreateTO);
        messageProducer.sendMessage(signContractRequestTopic, messageMapper.convertEventIntoStringMessage(event));
        return ContractMapper.INSTANCE.mapToTO(event, ContractAgreementStatusType.INITIATED);
    }

    @Transactional(readOnly = true)
    public ContractAgreementTO getContractAgreement(UUID contractAgreementId) {
        ContractAgreement contractAgreement = contractAgreementRepository.findById(contractAgreementId).orElseThrow(
                () -> new RecordNotFoundException("ContractAgreement", "contractAgreementId", contractAgreementId.toString())
        );
        return ContractAgreementMapper.INSTANCE.mapToTO(contractAgreement);
    }

    @Transactional(readOnly = true)
    public String getContractAgreementFile(UUID contractAgreementId) {
        ContractAgreement contractAgreement = contractAgreementRepository.findById(contractAgreementId).orElseThrow(
                () -> new RecordNotFoundException("ContractAgreement", "contractAgreementId", contractAgreementId.toString())
        );
        if (contractAgreement.getStatus() != ContractAgreementStatusType.FINALIZED) {
            throw new RecordNotFoundException("ContractAgreementFile", "contractAgreementId", contractAgreementId.toString());
        }
        return contractAgreementFileStorageService.readFile(contractAgreementId);
    }

    private static void validateContractAgreement(String contractDefinitionId, ContractAgreement contractAgreement) {
        if (!contractDefinitionId.equals(contractAgreement.getContractDefinitionId())) {
            throw new BadArgumentException("ContractDefinitionId is not the same: " + contractAgreement.getContractDefinitionId() + " " + contractDefinitionId);
        }
        if (contractAgreement.getStatus() == ContractAgreementStatusType.FINALIZED || contractAgreement.getStatus() == ContractAgreementStatusType.TERMINATED) {
            throw new BadArgumentException("Contract already finalized");
        }
    }
}
