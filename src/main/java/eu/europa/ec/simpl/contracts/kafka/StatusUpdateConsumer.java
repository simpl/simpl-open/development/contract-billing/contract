package eu.europa.ec.simpl.contracts.kafka;

import eu.europa.ec.simpl.contracts.kafka.events.StatusUpdateRequestEvent;
import eu.europa.ec.simpl.contracts.service.ContractAgreementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class StatusUpdateConsumer {

    private final MessageMapper messageMapper;
    private final ContractAgreementService contractAgreementService;

    public StatusUpdateConsumer(MessageMapper messageMapper, ContractAgreementService contractAgreementService) {
        this.messageMapper = messageMapper;
        this.contractAgreementService = contractAgreementService;
    }

    @KafkaListener(topics = "${spring.kafka.topics.status-update}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(String message) {
        log.info("Received message: " + message);
        StatusUpdateRequestEvent event = messageMapper.createEvent(message, StatusUpdateRequestEvent.class);
        contractAgreementService.updateContractStatus(event.getContractAgreementId(), event.getContractDefinitionId(), event.getNewStatus());
        log.info("Contract confirmed for ID: {}", event.getContractAgreementId());
    }
}
