package eu.europa.ec.simpl.contracts.kafka.events;

import eu.europa.ec.simpl.contracts.transfer.Mode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ContractAgreementResponseEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    private UUID contractAgreementId;
    private String contractNegotiationId;
    private Mode mode;
    private boolean signed;

}
