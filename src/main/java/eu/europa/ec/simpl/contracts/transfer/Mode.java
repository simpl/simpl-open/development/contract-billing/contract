package eu.europa.ec.simpl.contracts.transfer;

public enum Mode {

    CONSUMER,
    PROVIDER

}
