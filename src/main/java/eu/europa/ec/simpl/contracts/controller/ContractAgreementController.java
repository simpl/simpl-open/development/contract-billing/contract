package eu.europa.ec.simpl.contracts.controller;

import eu.europa.ec.simpl.contracts.service.ContractAgreementService;
import eu.europa.ec.simpl.contracts.transfer.ContractAgreementTO;
import eu.europa.ec.simpl.contracts.transfer.ContractResponseTO;
import eu.europa.ec.simpl.contracts.transfer.StatusUpdateRequestTO;
import eu.europa.ec.simpl.contracts.types.ContractAgreementStatusType;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/contract/v1/agreements")
public class ContractAgreementController {

    private final ContractAgreementService contractAgreementService;

    public ContractAgreementController(ContractAgreementService contractAgreementService) {
        this.contractAgreementService = contractAgreementService;
    }

    @PostMapping(value = "/{contractAgreementId}/definitions/{contractDefinitionId}/status", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContractResponseTO> updateContractAgreementStatus(@PathVariable(value = "contractAgreementId") UUID contractAgreementId,
                                                                            @PathVariable(value = "contractDefinitionId") String contractDefinitionId,
                                                                            @Valid @RequestBody StatusUpdateRequestTO statusUpdateRequestTO) {
        ContractAgreementStatusType statusType = ContractAgreementStatusType.valueOf(statusUpdateRequestTO.getStatus());
        if (statusType != ContractAgreementStatusType.FINALIZED && statusType != ContractAgreementStatusType.TERMINATED) {
            return ResponseEntity.badRequest().body(null);
        }
        return ResponseEntity.ok(contractAgreementService.sendStatusUpdateRequest(contractAgreementId, contractDefinitionId, statusType));
    }

    @GetMapping(value = "/{contractAgreementId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContractAgreementTO> getContractAgreement(@PathVariable(value = "contractAgreementId") UUID contractAgreementId) {
        return ResponseEntity.ok(contractAgreementService.getContractAgreement(contractAgreementId));
    }

    @GetMapping(value = "/file/{contractAgreementId}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> getContractAgreementFile(@PathVariable(value = "contractAgreementId") UUID contractAgreementId) {
        return ResponseEntity.ok(contractAgreementService.getContractAgreementFile(contractAgreementId));
    }
}
