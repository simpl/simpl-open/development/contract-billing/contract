package eu.europa.ec.simpl.contracts.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.exceptions.ResponseStatusSingleException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class MessageMapper {

    private final ObjectMapper objectMapper;

    public MessageMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T> T createEvent(String message, Class<T> clazz) {
        try {
            return objectMapper.readValue(message, clazz);
        } catch (JsonProcessingException e) {
            throw new ResponseStatusSingleException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to deserialize JSON message", e);
        }
    }

    public <T> String convertEventIntoStringMessage(T event) {
        try {
            return objectMapper.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            throw new ResponseStatusSingleException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to serialize JSON message", e);
        }
    }
}
