package eu.europa.ec.simpl.contracts.kafka;

import eu.europa.ec.simpl.common.service.ExternalRequestService;
import eu.europa.ec.simpl.contracts.kafka.events.ContractAgreementResponseEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Component
@Slf4j
public class SignContractResponseConsumer {

    private static final String CONFIRMATION_ENDPOINT = "/signed";
    private final MessageMapper messageMapper;
    private final ExternalRequestService externalRequestService;
    @Value("${spring.external-services.urls.edc}")
    private String edcHost;

    public SignContractResponseConsumer(MessageMapper messageMapper, ExternalRequestService externalRequestService) {
        this.messageMapper = messageMapper;
        this.externalRequestService = externalRequestService;
    }

    @KafkaListener(topics = "${spring.kafka.topics.sign-contract-response}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(String message) {
        log.info("Received message: " + message);
        ContractAgreementResponseEvent event = messageMapper.createEvent(message, ContractAgreementResponseEvent.class);
        log.info("Sending message about contract signature for: {} for ID: {}", event.getContractNegotiationId(), event.getContractAgreementId());
        // call EDC Connector Control Plane endpoint
        notifyEDC(event.getContractNegotiationId(), event.isSigned(), event.getContractAgreementId());
    }

    private void notifyEDC(String contractNegotiationId, boolean signed, UUID contractAgreementId) {
        String path = "%s/%s/%s".formatted(CONFIRMATION_ENDPOINT, contractNegotiationId, signed);
        WebClient.RequestBodySpec bodySpec = externalRequestService.getBodySpecPost(edcHost, path, null);
        Mono<String> response = bodySpec.retrieve().bodyToMono(String.class);
        log.info("Confirmation response for contractAgreementId {}: {}", contractAgreementId, response.block());
    }

}
