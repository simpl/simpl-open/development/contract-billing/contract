package eu.europa.ec.simpl.contracts.kafka.events;

import eu.europa.ec.simpl.contracts.transfer.Mode;
import eu.europa.ec.simpl.contracts.types.ContractAgreementStatusType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StatusUpdateRequestEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    private UUID contractAgreementId;
    private String contractDefinitionId;
    private Mode mode;
    private ContractAgreementStatusType newStatus;

}
