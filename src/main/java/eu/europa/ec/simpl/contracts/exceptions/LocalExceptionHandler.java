package eu.europa.ec.simpl.contracts.exceptions;

import eu.europa.ec.simpl.common.exceptions.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class LocalExceptionHandler extends GlobalExceptionHandler {

}
