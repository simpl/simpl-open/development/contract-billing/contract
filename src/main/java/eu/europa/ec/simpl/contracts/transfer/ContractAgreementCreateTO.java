package eu.europa.ec.simpl.contracts.transfer;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class ContractAgreementCreateTO implements Serializable {

    @NotNull
    private String contractNegotiationId;
    private String assetId;
    private String providerId;
    private String consumerId;
    private String contractOfferId;

}
