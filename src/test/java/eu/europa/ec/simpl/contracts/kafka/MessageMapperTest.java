package eu.europa.ec.simpl.contracts.kafka;

import eu.europa.ec.simpl.common.exceptions.ResponseStatusSingleException;
import eu.europa.ec.simpl.contracts.AbstractUnitTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import scala.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MessageMapperTest extends AbstractUnitTest {

    @Autowired
    private MessageMapper messageMapper;

    @Test
    void shouldFailToConvertEventIntoStringMessage() {
        //when
        ResponseStatusSingleException exception = assertThrows(ResponseStatusSingleException.class,
                () -> messageMapper.convertEventIntoStringMessage(new Object()));

        //then
        assertEquals("Failed to serialize JSON message", exception.getReason());
    }

    @Test
    void shouldFailToCreateEvent() {
        //when
        ResponseStatusSingleException exception = assertThrows(ResponseStatusSingleException.class,
                () -> messageMapper.createEvent("bigDecimal", BigDecimal.class));

        //then
        assertEquals("Failed to deserialize JSON message", exception.getReason());
    }
}
