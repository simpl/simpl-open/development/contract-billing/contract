package eu.europa.ec.simpl.contracts.service;

import eu.europa.ec.simpl.common.exceptions.BadArgumentException;
import eu.europa.ec.simpl.contracts.entity.ContractAgreement;
import eu.europa.ec.simpl.contracts.repository.ContractAgreementRepository;
import eu.europa.ec.simpl.contracts.types.ContractAgreementStatusType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Sql("/database/ContractAgreementServiceConfirmationData.sql")
@ExtendWith(OutputCaptureExtension.class)
@SpringBootTest(properties = "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration")
class ContractAgreementServiceConfirmationTest {

    @Autowired
    private ContractAgreementService contractAgreementService;

    @Autowired
    private ContractAgreementRepository contractAgreementRepository;

    @Test
    void shouldUpdateContractAgreementStatus() {
        //given
        UUID contractId = UUID.fromString("807fdb6b-a9de-430d-9bb2-c5909b4b2062");
        String definitionId = "807fdb6b-a9de-430d-9bb2-c5909b4b2064";

        //when
        contractAgreementService.updateContractStatus(contractId, definitionId, ContractAgreementStatusType.FINALIZED);

        //then
        List<ContractAgreement> dbData = contractAgreementRepository.findAll();
        assertThat(dbData)
                .hasSize(4)
                .extracting(ContractAgreement::getContractAgreementId,
                        ContractAgreement::getContractDefinitionId,
                        ContractAgreement::getStatus)
                .contains(
                        tuple(contractId, definitionId, ContractAgreementStatusType.FINALIZED)
                );
        ContractAgreement contractAgreement = contractAgreementRepository.findById(contractId).get();
        assertNotNull(contractAgreement.getConsumerSignatureDate());
        assertNotNull(contractAgreement.getProviderSignatureDate());
    }

    @Test
    void shouldFailForNotMatchingDefinitionId() {
        //given
        UUID contractId = UUID.fromString("807fdb6b-a9de-430d-9bb2-c5909b4b2063");
        String definitionId = "807fdb6b-a9de-430d-9bb2-c5909b4b2067";

        // when
        BadArgumentException exception = assertThrows(BadArgumentException.class,
                () -> contractAgreementService.updateContractStatus(contractId, definitionId, ContractAgreementStatusType.FINALIZED));

        // then
        assertEquals("ContractDefinitionId is not the same: 807fdb6b-a9de-430d-9bb2-c5909b4b2064" +
                " 807fdb6b-a9de-430d-9bb2-c5909b4b2067", exception.getReason());
    }

    @Test
    void shouldFailBecauseAlreadyFinalized() {
        //given
        UUID contractId = UUID.fromString("807fdb6b-a9de-430d-9bb2-c5909b4b2061");
        String definitionId = "807fdb6b-a9de-430d-9bb2-c5909b4b2064";

        // when
        BadArgumentException exception = assertThrows(BadArgumentException.class,
                () -> contractAgreementService.updateContractStatus(contractId, definitionId, ContractAgreementStatusType.FINALIZED));

        // then
        assertEquals("Contract already finalized", exception.getReason());
    }

    @Test
    void shouldFailIfContractAgreementNotExists(CapturedOutput output) {
        //given
        UUID contractId = UUID.fromString("807fdb6b-a9de-430d-9bb2-c5909b4b2065");
        String definitionId = "807fdb6b-a9de-430d-9bb2-c5909b4b2064";

        // when
        contractAgreementService.updateContractStatus(contractId, definitionId, ContractAgreementStatusType.FINALIZED);

        // then
        await().until(() -> output.getOut().contains("ContractAgreement not found for contractAgreementId: 807fdb6b-a9de-430d-9bb2-c5909b4b2065"));
    }
}
