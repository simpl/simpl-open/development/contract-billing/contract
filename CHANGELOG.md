# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2025-02-02

### Changed
- SIMPL-9162 Make kafka topics configurable
- java version in dockerfile + logs (use contractAgreementId everywhere)

## [1.0.0] - 2024-12-20

### Changed
- (SIMPL-8552) Fix sonar issue

## [0.0.4] - 202?-??-??

### Changed
- (SIMPL-8135) Fix fortify vulnerabilities; vulnerable dependencies handled
- (SIMPL-8864) Adjust vault integration in contract helm

### Added
- (SIMPL-8359) New GET endpoint for contract file + changelog format

### Changed
- (SIMPL-8157) Change package names and error handler
