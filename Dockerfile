FROM eclipse-temurin:21-jdk-alpine
COPY /target/*.jar simpl-contracts.jar

RUN mkdir /files && chmod 775 /files

RUN adduser -u 8877 -D dockeruser

RUN chown -R dockeruser:dockeruser /files

USER dockeruser

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "simpl-contracts.jar"]
